//
//  Extensions.swift
//  BarSearch
//
//  Created by Vladimir on 18/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let barListUpdatedNotification = Notification.Name("barListUpdated")
    static let showAlertNotification = Notification.Name("showAlert")
}
