//
//  GetBarSearchAPI.swift
//  BarSearch
//
//  Created by Vladimir on 19/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//

import Foundation
import CoreLocation

class BarSearchGetAPI: BaseAPI {
    var location: CLLocationCoordinate2D!
    var radius: Int!

    override var path: String {
        return "/venues/search"
    }

    override var parameters: [String: String] {
        var parameters = super.parameters
        parameters["ll"] = "\(location.latitude),\(location.longitude)"
        parameters["radius"] = "\(radius ?? 5000)"
        parameters["intent"] = "browse"
        parameters["categoryId"] = "4bf58dd8d48988d116941735"
        parameters["limit"] = "30"
        return parameters
    }

    func performRequest(compleation: (([BarModel]?) -> Void)?) {
        let task = URLSession.shared.dataTask(with: NSURL(string: url)! as URL, completionHandler: {(data, response, error) -> Void in
            let decoder = JSONDecoder()
            var bars: [BarModel]?
            do {
                let result = try decoder.decode(ResponceObject.self, from: data!)
                bars = result.response.venues
            } catch {
                fatalError("json error: \(error)")
            }
            DispatchQueue.main.async {
                compleation!(bars)
            }
        })
        task.resume()
    }
}
