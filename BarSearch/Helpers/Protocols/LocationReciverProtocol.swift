//
//  LocationReciverProtocol.swift
//  BarSearch
//
//  Created by Vladimir on 20/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationReciver: class {
    func newLocationRecived(_ location: CLLocationCoordinate2D?)
}
