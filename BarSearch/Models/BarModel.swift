//
//  BarModel.swift
//  BarSearch
//
//  Created by Vladimir on 17/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//

import Foundation
import CoreData
import MapKit

class BarModel: NSObject, Decodable {
    var id: String?
    var name: String?
    var contact: ContactModel?
    var location: LocationModel?
    var stats: StatsModel?
}

extension BarModel: MKAnnotation {
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: self.location?.lat ?? 0, longitude: self.location?.lng ?? 0)
    }
}
