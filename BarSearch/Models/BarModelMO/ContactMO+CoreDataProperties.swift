//
//  ContactMO+CoreDataProperties.swift
//  BarSearch
//
//  Created by Vladimir on 18/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//
//

import Foundation
import CoreData

extension ContactMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ContactMO> {
        return NSFetchRequest<ContactMO>(entityName: "ContactMO")
    }

    @NSManaged public var phone: String?
    @NSManaged public var formatedPhone: String?
    @NSManaged public var venue: BarModelMO?

}
