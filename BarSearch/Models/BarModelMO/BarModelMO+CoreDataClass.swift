//
//  BarModelMO+CoreDataClass.swift
//  BarSearch
//
//  Created by Vladimir on 18/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//
//

import Foundation
import CoreData

@objc(BarModelMO)
public class BarModelMO: NSManagedObject {
    var barModel: BarModel {
        let model = BarModel()
        model.id = self.id
        model.name = self.name
        model.contact = ContactModel()
        model.contact?.phone = self.contact?.phone
        model.contact?.formatedPhone = self.contact?.formatedPhone
        model.location = LocationModel()
        model.location?.address = self.location?.address
        model.location?.lat = self.location?.lat ?? 0
        model.location?.lng = self.location?.lng ?? 0
        model.location?.distance = self.location?.distance?.intValue
        model.location?.country = self.location?.country
        model.location?.city = self.location?.city
        model.stats = StatsModel()
        model.stats?.tipCount = self.stats?.tipCount.intValue
        model.stats?.usersCount = self.stats?.usersCount.intValue
        model.stats?.checkinsCount = self.stats?.checkinsCount.intValue
        return model
    }

}
