//
//  BarListController.swift
//  BarSearch
//
//  Created by Vladimir on 17/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//

import UIKit

class BarListController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchAgainButton: UIButton!
    let presenter = BarPresenter()

    @IBAction func searchAgainPressed(_ sender: UIButton) {
        presenter.updateBars()
    }

    @IBOutlet weak var spinner: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.view = self

        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: BSImages.mapIcon), for: .normal)
        button.addTarget(self, action: #selector(mapButtonPressed), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 53, height: 53)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        searchAgainButton.layer.borderColor = UIColor.gray.cgColor
        searchAgainButton.layer.borderWidth = 1.0
        navigationItem.title = BSStrings.listViewTitle
    }

    @objc func mapButtonPressed() {
        performSegue(withIdentifier: ReuseIdentifers.toMapSegue, sender: nil)
    }

}

extension BarListController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.barList?.count ?? 0
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifers.barCell, for: indexPath) as? BarTableViewCell else { return UITableViewCell() }
        cell.item = presenter.barList?[indexPath.row]
        return cell
    }

}

extension BarListController: BarSearchView {
    func searchStarted() {
        searchAgainButton.isEnabled = false
        spinner.startAnimating()
    }

    func barListReadyToPresent() {
        spinner.stopAnimating()
        searchAgainButton.isEnabled = true
        tableView.reloadData()
    }

}
